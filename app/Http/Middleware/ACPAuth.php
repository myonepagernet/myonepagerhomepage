<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Session;

class ACPAuth {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        if(Auth::guest()) {
            return redirect()->guest('shop/auth/login');
        } else if(Auth::user()->hasPermission('acp.access') == false) {
            return redirect('shop');
        } else if(Session::has('acp_login') == false && ($request->is('shop/acp') == false && $request->is('shop/acp/login') == false)) {
            return redirect('shop');
        }
        return $next($request);
    }

}
