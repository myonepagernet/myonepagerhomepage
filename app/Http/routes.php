<?php

//OnePager
Route::get('/', 'HomeController@index');

//Impressum
Route::get('legalnotice', 'HomeController@legal');
Route::get('shop/legalnotice', 'HomeController@legal');

//Shop Home
Route::get('shop', 'ShopController@index');

//Product
Route::get('shop/product/{id}', 'ShopController@showProduct');

//Kontoeinstellungen
Route::get('shop/my-account', 'KPanelController@index');
Route::get('shop/my-account/settings', 'KPanelController@index');
Route::post('shop/my-account/settings', 'KPanelController@settingsStore');
Route::get('shop/my-account/licenses', 'KPanelController@licenses');
Route::get('shop/my-account/licenses/{id}', 'KPanelController@licenseShow');
Route::get('shop/my-account/avatar', 'KPanelController@avatar');

//Kaufen
Route::get('shop/purchase/{id}', 'PurchaseController@update');
Route::get('shop/shopping-cart', 'PurchaseController@cart');

//ACP
Route::get('shop/acp', 'ACPController@index');
Route::get('shop/acp/login', 'ACPController@index');
Route::post('shop/acp/login', 'ACPController@login');
Route::get('shop/acp/logout', 'ACPController@logout');
Route::get('shop/acp/home', 'ACPController@home');
Route::get('shop/acp/user/search', 'ACPController@userSearch');
Route::post('shop/acp/user/search', 'ACPController@userSearchPost');
Route::get('shop/acp/user/list', 'ACPController@userList');
Route::get('shop/acp/user/edit/{id}', 'ACPController@userEdit');
Route::post('shop/acp/user/edit/{id}', 'ACPController@userEditPost');
Route::get('shop/acp/user/add', 'ACPController@userAdd');
Route::post('shop/acp/user/add', 'ACPController@userAddPost');

Route::controllers([
    'shop/auth' => 'Auth\AuthController',
    'shop/password' => 'Auth\PasswordController',
]);
