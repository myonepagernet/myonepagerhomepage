<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Auth;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Request;

class KPanelController extends Controller {
    
    public function __construct() {
        $this->middleware('auth');
        
        if(Auth::guest()) {
            return redirect('shop');
        }
        
        if(Auth::user()->activated == 0) {
            \Session::flash('warning_message', 'Dein Account wurde noch nicht aktiviert. Klicke <a href="#">hier</a> um die E-Mail erneut zu senden.');
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        return view('kpanel.home');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function licenses() {
        if(Auth::guest()) {
            return abort(403, "Forbidden");
        }
        $ch = curl_init(); 
        curl_setopt($ch, CURLOPT_URL, "http://lizenz.myonepager.net/user/" . Auth::user()->id);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
        $licences = json_decode(curl_exec($ch));
        curl_close($ch);

        if((isset($licences->message) && $licences->message == "NoLizenzFound") || !$licences){
            $licences = array();
        }
        
        return view('kpanel.licenses', compact('licences'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function settingsStore(Request $request) {
        Validator::extend('is_current_pw', function ($attribute, $value, $parameters) {
            return Hash::check($value, Auth::user()->password) == true;
        });
        Validator::extend('differs_fromold', function ($attribute, $value, $parameters) {
            return Hash::check($value, Auth::user()->password) == false;
        });
        
        $messages = [
            'is_current_pw' => 'Das aktuelle Passwort stimmt nicht überein.',
            'differs_fromold' => 'Das neue Passwort ist dem neuen Passwort gleich.'
        ];
        
        $validator = Validator::make($request->all(), [
            'current_password' => 'required|is_current_pw',
            'new_password' => 'min:6|confirmed|differs_fromold',
            'new_email' => 'email|confirmed|unique:users'
        ], $messages);
        
        if ($validator->fails()) {
            return redirect('shop/my-account/settings')->withErrors($validator);
        }
        
        $user = User::findOrFail(Auth::user()->id);
        $input = $request->all();
        if(isset($input["email"]) && $input["email"] != '') {
            $user->email = $input["email"];
        }
        if(isset($input["password"]) && $input["password"] != '') {
            $user->password = bcrypt($input["password"]);
        }
        $user->birthday = strtotime($input["birthday"]);
        $user->save();
        
        \Session::flash('success_message', 'Dein Profil wurde aktualisiert.');

        return redirect('shop/my-account/settings');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function licenseShow($id) {
        if(Auth::guest()) {
            return abort(403, "Forbidden");
        }
        $ch = curl_init(); 
        curl_setopt($ch, CURLOPT_URL, "http://lizenz.myonepager.net/user/" . Auth::user()->id);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
        $licences = json_decode(curl_exec($ch));
        curl_close($ch);
        
        if(isset($licences->message) && $licences->message == "NoLizenzFound"){
            return abort(404, "Not Found");
        }
        
        foreach($licences as $license) {
            if($license->lizenzId == $id) {
                return view('kpanel.license',  compact('license'));
            }
        }
        
        if($license->userId != Auth::user()->id) {
            return abort(403, "Forbidden");
        }
        
        return abort(404, "Not Found");
    }
}
