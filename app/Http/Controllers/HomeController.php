<?php

namespace App\Http\Controllers;

class HomeController extends Controller {

    /**
     * Show the application dashboard to the user.
     *
     * @return Response
     */
    public function index() {
        return view('home');
    }
    
    /**
     * Show the legal notice to the user
     * 
     * @return Response
     */
    public function legal(){
        return view('legal');
    }
}
