<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Product;
use Gloudemans\Shoppingcart\Facades\Cart;

class PurchaseController extends Controller {

	/**
	 * Displays Shopping-Cart
	 *
	 * @return Response
	 */
	public function cart() {
            return view('shop.shopping-cart');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id) {
            $content = Product::where('id', '=', $id)->firstOrFail();
            Cart::add($id, $content->name, 1, $content->price);
            
            return redirect('shop/shopping-cart');
	}
}
