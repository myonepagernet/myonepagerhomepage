<?php 

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Auth;
use Session;
use Illuminate\Http\Request;
use App\User;
use App\Group;

class ACPController extends Controller {
    
    public function __construct() {
        $this->middleware('ACPAuth');
    }
    
    public function index() {
        if (Session::has('acp_login') && Session::get('acp_login') == Auth::user()->id) {
            return redirect('shop/acp/home');
        }
        return view('acp.login');
    }

    public function login(Request $request) {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email', 
            'password' => 'required'
        ]);
        
        $credentials = $request->only('email', 'password');
        
        if ($validator->fails()) {
            return redirect('shop/acp/login')->withErrors($validator);
        } else if(Auth::validate($credentials) === false) {
            return redirect('shop/acp/login')->withErrors(array('message' => 'Die Daten sind inkorrekt.'));
        }
        
        Session::put('acp_login', Auth::user()->id);
        return redirect('shop/acp/home');
    }
    
    public function logout() {
        if(Session::has('acp_login')) {
            Session::forget('acp_login');
        }
        return redirect('shop');
    }
    
    public function home() {
        return view('acp.home');
    }
    
    public function userSearch() {
        return view('acp.user.search');
    }
    
    public function userSearchPost() {
        
    }
    
    public function userList() {
        $users = User::paginate(20);
        
        return view('acp.user.list', compact('users'));
    }
    
    public function userEdit($id) {
        $user = User::findOrFail($id);
        $groups = Group::all();
        
        unset($groups[0]);
        
        $user_groups = $user->groups->toArray();
        for($i = 0; $i < count($user_groups); $i++) {
            unset($user_groups[$i]["pivot"]);
        }
        
        return view('acp.user.edit', compact('user', 'groups', 'user_groups'));
    }
    
    public function userEditPost($id, Request $request) {
        $user = User::findOrFail($id);
        
        Validator::extend('is_unique', function($attribute, $value, $parameters) use ($user) {
            $unique = User::where('email', '=', $value);
            
            
            return ($unique->count() >= 1 && $unique != $user->email) == false;
        });
        
        $messages = array(
            'validation.isset_confirmed' => 'Die Passwörter stimmen nicht überein.'
        );
        
        Validator::extend('isset_confirmed', function($attribute, $value, $parameters) {
            global $request;
            if($value != null || $value != '') {
                return ($request->input('password') == $request->input('password_confirmation')) == true;
            }
            return true == true;
        });

        $validator = Validator::make($request->all(), [
            'username' => 'required|max:32|min:6',
            'email' => 'email|required|confirmed|is_unique:users',
            'password' => 'isset_confirmed'
        ], $messages);
        
        if($validator->fails()) {
            return redirect('shop/acp/user/edit/1')->withErrors($validator);
        }
    }

}
