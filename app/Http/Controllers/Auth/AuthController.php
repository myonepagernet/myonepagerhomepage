<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller {

    use AuthenticatesAndRegistersUsers;

    /**
     * Create a new authentication controller instance.
     *
     * @param  \Illuminate\Contracts\Auth\Guard  $auth
     * @param  \Illuminate\Contracts\Auth\Registrar  $registrar
     * @return void
     */
    public function __construct(Guard $auth, Registrar $registrar) {
        $this->auth = $auth;
        $this->registrar = $registrar;

        $this->middleware('guest', ['except' => 'getLogout']);
    }
    
    public function getActivate($id) {
        if(!$id) {
            throw new InvalidActivationTokenException;
        }
        
        $user = User::where('activationToken', '=', $id)->first();
        
        if(!$user) {
            throw new InvalidActivationTokenException;
        }
        
        $user->activated = 1;
        $user->activationToken = null;
        $user->save();
        
        \Session::flash('success_message', 'Dein Account wurde erfolgreich aktiviert.');

        return redirect('shop/my-account/settings');
    }

}
