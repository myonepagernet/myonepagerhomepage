<?php namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name', 'email', 'password', 'activated', 'activationToken', 'birthday'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];
        
        public function groups() {
            return $this->belongsToMany('App\Group');
        }
        
        public function hasPermission($permission) {
            if(!$this->groups) {
                return false;
            }
            
            $groups = $this->groups->toArray();
            
            foreach($groups as $group) {
                $group_perms = Group::findOrFail($group["id"])->permissions->toArray();
                
                foreach($group_perms as $group_perm) {
                    if($group_perm["name"] == $permission) {
                        return true;
                    }
                }
            }
            
            return false;
        }
}
