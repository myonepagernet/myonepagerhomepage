<?php

namespace App\Services;

use App\User;
use Validator;
use Illuminate\Contracts\Auth\Registrar as RegistrarContract;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class Registrar implements RegistrarContract {

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function validator(array $data) {
        Validator::extend('captcha_check', function ($attribute, $value, $parameters) {
            $post_url = 'https://www.google.com/recaptcha/api/siteverify';
            $data = array('secret' => '6LfnOAQTAAAAALp05j9daqyC76yxOFVr5VwmmiXb', 'response' => Input::get('g-recaptcha-response'), 'remoteip', $_SERVER["REMOTE_ADDR"]);

            $options = array(
                'http' => array(
                    'header' => "Content-type: application/x-www-form-urlencoded\r\n",
                    'method' => 'POST',
                    'content' => http_build_query($data),
                ),
            );
            $context = stream_context_create($options);
            $result = json_decode(file_get_contents($post_url, false, $context));

            return $result->success == true;
        });

        $messages = [
            'captcha_check' => 'Die überprüfung des Captchas ist fehlgeschlagen'
        ];

        return Validator::make($data, [
                    'name' => 'required|max:255',
                    'email' => 'required|email|max:255|unique:users|confirmed',
                    'password' => 'required|confirmed|min:6',
                    'birthday' => 'required|date',
                    'g-recaptcha-response' => 'captcha_check'
                        ], $messages);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    public function create(array $data) {
        $activationtoken = str_random(30);

        $user = User::create([
                    'name' => $data['name'],
                    'email' => $data['email'],
                    'password' => bcrypt($data['password']),
                    'birthday' => strtotime($data['birthday']),
                    'activationToken' => $activationtoken
        ]);

        Mail::send('emails.activation', ['name' => $data['name'], 'token' => $activationtoken], function($m) use ($data) {
            $m->to($data['email'])->subject('Willkommen auf MyOnePager.net!');
        });

        DB::table('group_user')->insert([
            'group_id' => 1,
            'user_id' => $user->id
        ]);

        return $user;
    }

}
