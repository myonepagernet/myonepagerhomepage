@extends('onepager')

@section('content')
<!-- Header -->
<a name="about"></a>
<div class="intro-header">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="intro-message">
                    <h1>MyOnePager.net</h1>
                    <h3>Deine Software für Deinen One-Pager</h3>
                    <hr class="intro-divider">
                    <ul class="list-inline intro-social-buttons">
                        <li>
                            <a href="#" class="btn btn-default btn-lg"><i class="fa fa-twitter fa-fw"></i> <span class="network-name">Twitter</span></a>
                        </li>
                        <li>
                            <a href="#" class="btn btn-default btn-lg"><i class="fa fa-facebook fa-fw"></i> <span class="network-name">Facebook</span></a>
                        </li>
                        <li>
                            <a href="#" class="btn btn-default btn-lg"><i class="fa fa-github fa-fw"></i> <span class="network-name">Github</span></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<a name="services"></a>
<div class="content-section-a">
    <div class="container">
        <div class="row">
            <div class="col-lg-5 col-sm-6">
                <hr class="section-heading-spacer">
                <div class="clearfix"></div>
                <h2 class="section-heading">MyOnePager</h2>
                <p class="lead">MyOnePager ist eine Software für die Erstellungen eines eigenen One-Pagers. Sie ist einfach zu bedienen und hat viele verschiedene einstellungen. Durch eine Administrative Oberfläche lässt sich alles steuern.</p>
                <a href="{{ url('shop') }}" class="btn btn-success btn-block">Zum Shop</a>
            </div>
            <div class="col-lg-5 col-lg-offset-2 col-sm-6">
                <img class="img-responsive" src="images/macbook_acp_login.png" alt="">
            </div>
        </div>
    </div>
</div>

<a name="contact"></a>
<div class="content-section-b">
    <div class="container">
        <div class="row">
            <div class="col-lg-5 col-sm-6">
                <hr class="section-heading-spacer">
                <div class="clearfix"></div>
                <h2 class="section-heading">Responsive</h2>
                <p class="lead">Egal ob Handy, Tablet, Fernsehr oder Computer, MyOnePager ist auf die gängisten Geräte angepasst. Dank Bootstrap ermöglichen wir ein schlichtes und einfach zu erstellendes Design für viele Geräte.</p>
            </div>
            <div class="col-lg-5 col-lg-offset-2 col-sm-6">
                <img class="img-responsive" src="images/phone_responsive.png" alt="">
            </div>
        </div>
    </div>
</div>

<a name="contact"></a>
<div class="content-section-a">
    <div class="container">
        <hr class="section-heading-spacer">
        <div class="clearfix"></div>
        <h2 class="section-heading">Kontakt</h2>
        <p class="lead">Noch fragen? Melden Sie sich eifnach in unserem Shop an und kotaktieren Sie uns über unserer Support-System.</p>
        <div class="row">
            <div class="col-lg-5 col-sm-6">
                <a href="{{ url('shop/auth/login') }}" class="btn btn-success btn-block">Zum Kundenportal</a>
            </div>
        </div>
    </div>
</div>

<footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <ul class="list-inline">
                    <li>
                        <a href="#">Home</a>
                    </li>
                    <li class="footer-menu-divider">&sdot;</li>
                    <li>
                        <a href="#about">Über Uns</a>
                    </li>
                    <li class="footer-menu-divider">&sdot;</li>
                    <li>
                        <a href="#services">Produkte</a>
                    </li>
                    <li class="footer-menu-divider">&sdot;</li>
                    <li>
                        <a href="#contact">Kontakt</a>
                    </li>
                    <li class="footer-menu-divider">&sdot;</li>
                    <li>
                        <a href="#contact">Zum Shop</a>
                    </li>
                    <li class="footer-menu-divider">&sdot;</li>
                    <li>
                        <a href="{{ url('legalnotice') }}">Impressum</a>
                    </li>
                </ul>
                <p class="copyright text-muted small">Copyright &copy; MyOnePager.net 2014. All Rights Reserved</p>
            </div>
        </div>
    </div>
</footer>
@endsection
