<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>MyOnePager.net - ACP</title>

        <link href="{{ asset('/css/bootstrap.css') }}" rel="stylesheet">
        <link href="{{ asset('/css/style.css') }}" rel="stylesheet">
        <link href="{{ asset('/css/acp.css') }}" rel="stylesheet">
        <link href="{{ asset('/css/font-awesome.min.css') }}" rel="stylesheet">

        <!-- Fonts -->
        <link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
                <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
                <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <script src='https://www.google.com/recaptcha/api.js'></script>
    </head>
    <body> 
        <!-- Navigation -->
        <nav class="navbar navbar-default" role="navigation" style="margin-bottom: 0;">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand topnav" href="{{ url('shop/acp/home') }}">MyOnePager.net - ACP</a>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    @if(Session::has('acp_login'))

                    @endif
                </ul>
            </div>
        </nav>

        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="side-menu">
                    @if(Session::has('acp_login'))
                    <li class="{{ (Request::is('shop/acp/user/*')) ? 'active' : '' }}">
                        <a href=""><i class="fa fa-users"></i> <span class="nav-label">Benutzer</span> <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse {{ (Request::is('shop/acp/user/*')) ? 'in' : '' }}">
                            <li class="{{ (Request::is('shop/acp/user/search')) ? 'active' : '' }}"><a href="{{ url('shop/acp/user/search') }}">Benutzer suchen</a></li>
                            <li class="{{ (Request::is('shop/acp/user/list')) ? 'active' : '' }}"><a href="{{ url('shop/acp/user/list') }}">Benutzer auflisten</a></li>
                            <li class="{{ (Request::is('shop/acp/user/add')) ? 'active' : '' }}"><a href="{{ url('shop/acp/user/add') }}">Benutzer hinzufügen</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-sitemap"></i> <span class="nav-label">Menu Levels </span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">
                            <li>
                                <a href="#">Third Level <span class="fa arrow"></span></a>
                                <ul class="nav nav-third-level collapse">
                                    <li>
                                        <a href="#">Third Level Item</a>
                                    </li>
                                    <li>
                                        <a href="#">Third Level Item</a>
                                    </li>
                                    <li>
                                        <a href="#">Third Level Item</a>
                                    </li>

                                </ul>
                            </li>
                            <li><a href="#">Second Level Item</a></li>
                            <li>
                                <a href="#">Second Level Item</a></li>
                            <li>
                                <a href="#">Second Level Item</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="{{ url('shop/acp/logout') }}"><i class="fa fa-sign-out"></i> <span class="nav-label">Abmelden</span></a>
                    </li>
                    @else
                    <li class="{{ (Request::is('acp') || Request::is('acp/login')) ? '' : 'active' }}">
                        <a href="{{ url('shop/acp/login') }}"><i class="fa fa-sign-in"></i> <span class="nav-label">Anmelden</span></a>
                    </li>
                    @endif
                </ul>
            </div>
        </nav>

        <!-- Content -->
        <div class="page-wrapper">
            @yield('content')
        </div>

        <!-- Scripts -->
        <script src="{{ asset('/js/jquery.js') }}"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/metisMenu/1.1.3/metisMenu.min.js"></script>
        <script>
            $('#side-menu').metisMenu();
        </script>
    </body>
</html>
