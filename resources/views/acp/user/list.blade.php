@extends('acp.acp')

@section('content')
<div class="container" style="margin-top: 5%;">
    <div class="panel panel-default">
        <div class="panel-heading">Benutzer <span class="badge">{{ count($users) }}</span></div>
        <div class="panel-body">
            <table class="table">
                <thead>
                    <tr>
                        <th colspan="2">ID</th>
                        <th>Benutzername</th>
                        <th>E-Mail Adresse</th>
                        <th>Registrierungsdatum</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($users as $user)
                    <tr>
                        <td>
                            <a href="{{ url('shop/acp/user/edit/' . $user->id) }}"><span title="Bearbeiten"><i class="fa fa-pencil"></i></span></a>&nbsp;
                            @if(Auth::user()->id == $user->id)
                                <span title="Löschen"><i class="fa fa-times"></i></span>
                            @else
                                <a href="{{ url('shop/acp/user/delete/' . $user->id) }}"><span title="Löschen"><i class="fa fa-times"></i></span></a>
                            @endif
                        </td>
                        <td>{{ $user->id }}</td>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $user->created_at }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            {{ $users->render() }}
        </div>
    </div>
</div>
@endsection