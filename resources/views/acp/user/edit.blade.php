@extends('acp.acp')

@section('content')
<div class="container" style="margin-top: 5%;">
    {!! Form::open() !!}
    @if (count($errors) > 0)
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="alert alert-danger" role="alert">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
    @endif

    <ul class="nav nav-tabs nav-justified">
        <li role="presentation" class="active"><a href="#data" aria-controls="data" role="tab" data-toggle="tab">Allgemeine Daten</a></li>
        <li role="presentation"><a href="#pdata" aria-controls="pdata" role="tab" data-toggle="tab">Persönliche Daten</a></li>
    </ul>

    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="data">
            <div class="panel panel-default">
                <div class="panel-body">
                    <legend>Benutzer</legend>
                    <div class="form-group">
                        <label for="username" class="col-sm-2 control-label">Benutzername</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="username" name="username" value="{{ $user->name }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="username" class="col-sm-2 control-label">Benutzergruppe(n)</label>
                        <div class="col-sm-10">
                            @foreach($groups as $group)
                            <div class="checkbox">
                                @if(in_array($group->toArray(), $user_groups))
                                <label><input type="checkbox" name="groupIDs[]" value="{{ $group->id }}" checked="checked"> {{ $group->name }}</label>
                                @else
                                <label><input type="checkbox" name="groupIDs[]" value="{{ $group->id }}"> {{ $group->name }}</label>
                                @endif
                            </div>
                            @endforeach
                        </div>
                    </div>
                    <legend>E-Mail Adresse</legend>
                    <div class="form-group">
                        <label for="email" class="col-sm-2 control-label">E-Mail Adresse</label>
                        <div class="col-sm-10">
                            <input type="email" class="form-control" id="email" name="email" value="{{ $user->email }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="email_confirmation" class="col-sm-2 control-label">E-Mail Adresse wiederholen</label>
                        <div class="col-sm-10">
                            <input type="email" class="form-control" id="email_confirmation" name="email_confirmation" value="{{ $user->email }}">
                        </div>
                    </div>
                    <legend>Kennwort</legend>
                    <div class="form-group">
                        <label for="password" class="col-sm-2 control-label">Passwort</label>
                        <div class="col-sm-10">
                            <input type="password" class="form-control" id="password" name="password">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="password_confirmation" class="col-sm-2 control-label">Passwort wiederholen</label>
                        <div class="col-sm-10">
                            <input type="password" class="form-control" id="password_confirmation" name="password_confirmation">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="pdata">
            Folgt...
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-body">
            <input type="submit" class="btn btn-primary btn-block" value="Speichern">
        </div>
    </div>
    {!! Form::open() !!}
</div>
@endsection