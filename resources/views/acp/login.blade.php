@extends('acp.acp')

@section('content')
<div class="container" style="margin-top: 5%;">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="login-panel panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Admin Login</h3>
                </div>
                <div class="panel-body">
                    {!! Form::open(array('class' => 'form-signin', 'url' => 'shop/acp/login')) !!}
                        <fieldset>
                            @if (count($errors) > 0)
                            <div class="alert alert-danger" role="alert">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif
                            <div class="form-group">
                                <label for="email">E-Mail Adresse:</label>
                                <input class="form-control" type="email" id="email" name="email" placeholder="E-Mail Adresse" value="{{ Auth::user()->email }}" required>
                            </div>
                            <div class="form-group">
                                <label for="password">Passwort:</label>
                                <input class="form-control" type="password" id="password" name="password" placeholder="Passwort" value="" autofocus required>
                            </div>
                            <input class="btn btn-lg btn-success btn-block" type="submit" name="submit" value="Anmelden">
                        </fieldset>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection