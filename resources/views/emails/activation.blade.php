Sehr geehrter {{ $name }},<br>
um Ihren Account aktivieren zu können, klicken sie auf folgenden Link: <a href="{{ url('shop/auth/activate/'.$token) }}">hier</a>.<br>
Falls dieser Link nicht funktioniert, klicken sie auf folgenden Link: {{ url('shop/auth/activate/'.$token) }}<br><br>

Mit freundlichen Grüßen<br>
MyOnePager.net - Team<br><br>

<hr>
Bitte antworten Sie nicht auf diese E-Mail. Diese wurde vom System automatisch generiert.