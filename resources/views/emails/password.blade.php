Sehr geehrter Nutzer,<br>
um ihr Passwort auf MyOnePager.net zurückzusetzen, klicken sie <a href="{{ url('shop/password/reset/'.$token) }}">hier</a>.<br>
Falls dieser Link nicht funktioniert, klicken sie auf folgenden Link: {{ url('shop/password/reset/'.$token) }}<br><br>

Mit freundlichen Grüßen<br>
MyOnePager.net - Team<br><br>

<hr>
Bitte antworten Sie nicht auf diese E-Mail. Diese wurde vom System automatisch generiert.