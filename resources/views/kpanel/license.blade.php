@extends('app')

@section('content')
<div class="container">
    <div class="row">
        <!-- Side Navigation -->
        <div class="col-xs-6 col-md-4">
            @include('kpanel.sidenav')
        </div>
        <!-- Content -->
        <div class="col-xs-12 col-sm-6 col-md-8">
            <legend><h2>Lizenz Nr. {{ $license->lizenzId }}</h2></legend>
            <div class="well well-sm">
                <legend><h4>Details</h4></legend>
                <table class="table-condensed">
                    <tbody>
                        <tr>
                            <td class="text-right"><strong>Produkte:</strong></td>
                            <td>MyOnePager &reg; ({{ $license->lizenzVersion }})<br></td>
                        </tr>
                        <tr>
                            <td class="text-right"><strong>Seriennummer:</strong></td>
                            <td>{{ substr($license->lizenzSchluessel, 0, 20) }}<br></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <h4>Lizenz bearbeiten</h4>
            
            <div class="well well-sm">
                <legend><h4>Domain für diese Lizenz</h4></legend>
                <form method="post" action="#" class="form-horizontal">
                    <input type="hidden" value="{{ csrf_token() }}">
                    <div class="form-group">
                        <label for="domain" class="col-sm-2 control-label">Domain</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="domain" name="domain" value="{{ $license->lizenzUrl }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-success btn-xs">Absenden</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection