<ul class="nav nav-pills nav-stacked">
    <li role="presentation" class="{{ (Request::is('shop/my-account/settings') || Request::is('shop/my-account')) ? 'active' : '' }}"><a href="{{ url('/shop/my-account/settings') }}">Verwalten</a></li>
    <li role="presentation" class="{{ Request::is('shop/my-account/avatar') ? 'active' : '' }}"><a href="{{ url('/shop/my-account/avatar') }}">Avatar</a></li>
    <li role="presentation" class="{{ (Request::is('shop/my-account/licenses')) ? 'active' : '' }}"><a href="{{ url('/shop/my-account/licenses') }}">Meine Lizenzen</a></li>
</ul>