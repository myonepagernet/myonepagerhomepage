@extends('app')

@section('content')
<div class="container">
    <div class="row">
        <!-- Side Navigation -->
        <div class="col-xs-6 col-md-4">
            @include('kpanel.sidenav')
        </div>
        <!-- Content -->
        <div class="col-xs-12 col-sm-6 col-md-8">
            <legend><h1>Lizenzen</h1></legend>

            @if(is_array($licences))
            @foreach($licences as $license)
            <div class="well">
                <legend><h4>Lizenz Nr. {{ $license->lizenzId }}</h4></legend>
                <table class="table-condensed">
                    <tbody>
                        <tr>
                            <td class="text-right"><strong>Produkte:</strong></td>
                            <td>MyOnePager &reg; ({{ $license->lizenzVersion }})<br></td>
                        </tr>
                        <tr>
                            <td class="text-right"><strong>Seriennummer:</strong></td>
                            <td>{{ substr($license->lizenzSchluessel, 0, 20) }}<br></td>
                        </tr>
                        <tr>
                            <td class="text-right"><strong>Registrierte Domain:</strong></td>
                            <td>{{ ($license->lizenzUrl == '' || is_null($license->lizenzUrl)) ? 'Keine' : $license->lizenzUrl }}<br></td>
                        </tr>
                    </tbody>
                </table><br>
                <a href="{{ url('shop/my-account/licenses/' . $license->lizenzId) }}" class="btn btn-default btn-xs">Mehr Details</a>
            </div>
            @endforeach
            @else
            <div class="alert alert-warning" role="alert">
                Sie besitzen keine Lizenzen.
            </div>
            @endif
        </div>
    </div>
</div>
@endsection