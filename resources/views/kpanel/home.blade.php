@extends('app')

@section('content')
<div class="container">
    <div class="row">
        <!-- Side Navigation -->
        <div class="col-xs-6 col-md-4">
            @include('kpanel.sidenav')
        </div>
        <!-- Content -->
        <div class="col-xs-12 col-sm-6 col-md-8">
            @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Whoops!</strong> Es gab Probleme mit deinen Angaben.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            
            @if (Session::has('success_message'))
            <div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                {{ Session::get('success_message') }}
            </div>
            @endif
            
            @if (Session::has('warning_message'))
            <div class="alert alert-warning" role="alert">
                {{ Session::get('warning_message') }}
            </div>
            @endif
            
            <legend><h1>Einstellungen</h1></legend>
            {!! Form::open(array('class' => 'form-horizontal')) !!}
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                
                <div class="form-group">
                    <label for="current_password" class="col-sm-2 control-label"><b>Aktuelles Passwort</b></label>
                    <div class="col-sm-10">
                        <input type="password" class="form-control" id="current_password" name="current_password" placeholder="Aktuelles Passwort" required>
                    </div>
                </div>
                <hr>
                <div class="form-group">
                    <label for="password" class="col-sm-2 control-label"><b>Neues Passwort</b></label>
                    <div class="col-sm-10">
                        <input type="password" class="form-control" id="password" name="password" placeholder="Neues Passwort">
                    </div>
                </div>
                <div class="form-group">
                    <label for="password_confirmation" class="col-sm-2 control-label"><b>Neues Passwort wiederholen</b></label>
                    <div class="col-sm-10">
                        <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" placeholder="Neues Passwort wiederholen">
                    </div>
                </div>
                <hr>
                <div class="form-group">
                    <label class="col-sm-2 control-label"><b>Aktuelle E-Mail</b></label>
                    <div class="col-sm-10">
                        <p class="form-control-static">{{ Auth::user()->email }}</p>
                    </div>
                </div>
                <div class="form-group">
                    <label for="email" class="col-sm-2 control-label"><b>Neue E-Mail Adresse</b></label>
                    <div class="col-sm-10">
                        <input type="email" class="form-control" id="email" name="email" placeholder="Neue E-Mail Adresse">
                    </div>
                </div>
                <div class="form-group">
                    <label for="email_confirmation" class="col-sm-2 control-label"><b>Neue E-Mail Adresse wiederholen</b></label>
                    <div class="col-sm-10">
                        <input type="email" class="form-control" id="email_confirmation" name="email_confirmation" placeholder="Neue E-Mail Adresse wiederholen">
                    </div>
                </div>
                <hr>
                <div class="form-group">
                    <label for="birthday" class="col-sm-2 control-label"><b>Geburtstag</b></label>
                    <div class="col-sm-10">
                        <input type="date" class="form-control" id="birthday" name="birthday" value="{{ date('Y-m-d', Auth::user()->birthday) }}">
                    </div>
                </div>
                <hr>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-default">Abspeichern</button>
                    </div>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection