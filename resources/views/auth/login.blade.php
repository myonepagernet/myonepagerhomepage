@extends('app')

@section('content')
<div class="container">
    <div class="card card-container">
        <img id="profile-img" class="profile-img-card" src="//ssl.gstatic.com/accounts/ui/avatar_2x.png" />
        <p id="profile-name" class="profile-name-card"></p>
        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> Es gab Probleme mit deinen Angaben.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        <form class="form-signin" role="form" method="POST" action="{{ url('shop/auth/login') }}">
            <span id="reauth-email" class="reauth-email"></span>
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="email" name="email" value="{{ old('email') }}" class="form-control" placeholder="E-Mail Adresse" required autofocus>
            <input type="password" name="password" class="form-control" placeholder="Passwort" required>
            <div id="remember" class="checkbox">
                <label>
                    <input type="checkbox" value="remember-me" name="remember"> Dauerhaft angemeldet bleiben
                </label>
            </div>
            <button class="btn btn-lg btn-primary btn-block btn-signin" type="submit">Anmelden</button>
        </form>
        <a href="{{ url('/shop/password/email') }}" class="forgot-password">
            Passwort vergessen?
        </a>
    </div>
</div>
@endsection
