@extends('app')

@section('content')
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
            <div class="panel-heading">Registrieren</div>
            <div class="panel-body">
                @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> Es gab Probleme mit deinen Angaben.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif

                <form class="form-horizontal" role="form" method="POST" action="{{ url('/shop/auth/register') }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <div class="form-group">
                        <label class="col-md-4 control-label">Benutzername</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="name" value="{{ old('name') }}">
                        </div>
                    </div>

                    <hr>

                    <div class="form-group">
                        <label class="col-md-4 control-label">E-Mail Adresse</label>
                        <div class="col-md-6">
                            <input type="email" class="form-control" name="email" value="{{ old('email') }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label">E-Mail Adresse wiederholen</label>
                        <div class="col-md-6">
                            <input type="email" class="form-control" name="email_confirmation" value="{{ old('email') }}">
                        </div>
                    </div>

                    <hr>

                    <div class="form-group">
                        <label class="col-md-4 control-label">Passwort</label>
                        <div class="col-md-6">
                            <input type="password" class="form-control" name="password">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label">Passwort wiederholen</label>
                        <div class="col-md-6">
                            <input type="password" class="form-control" name="password_confirmation">
                        </div>
                    </div>

                    <hr>

                    <div class="form-group">
                        <label class="col-md-4 control-label">Geburtstag</label>
                        <div class="col-md-6">
                            <input type="date" class="form-control" name="birthday" max="{{ date('Y-m-d') }}">
                        </div>
                    </div>

                    <hr>

                    <div class="form-group">
                        <label class="col-md-4 control-label">Captcha</label>
                        <div class="col-md-6">
                            <div class="g-recaptcha" data-sitekey="6LfnOAQTAAAAAL3YgwKkZSqfnay_MmgpmqUhSFBi"></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button type="submit" class="btn btn-primary">
                                Registrieren
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
