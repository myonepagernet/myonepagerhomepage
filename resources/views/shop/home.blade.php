@extends('app')

@section('content')
<div class="container">
    <div>
        <div class="page-header">
            <h3><b>MyOnePager GmbH</b><br><small>Deine Software für Deinen One-Pager</small></h3>
        </div>
        @foreach($products as $product)
            @if($product["main"] == 1)
                <div class="well">
                    <div class="row">
                        <div class="col-md-6">
                            <h4><b>{{ $product["name"] }}</b></h4>
                            <p>{{ $product["description"] }}</p>
                            <ul class="list-inline text-center">
                                <li><a href="{{ url('shop/product/' . $product["link"]) }}" class="btn btn-default btn-xs">Mehr Informationen</a></li>
                                <li><a href="#" class="btn btn-default btn-xs">Demo ausprobieren</a></li>
                                <li><a href="{{ url('shop/purchase/' . $product["id"]) }}" class="btn btn-info btn-xs">{{ $product["name"] }} kaufen</a></li>
                            </ul>
                        </div>
                        <div class="col-md-6">
                            <img src="{{ $product["img_path"] }}" alt="">
                        </div>
                    </div>
                </div>
            @endif
        @endforeach
    </div>
</div>
@endsection