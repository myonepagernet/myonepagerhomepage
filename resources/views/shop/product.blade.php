@extends('app')

@section('content')
<div class="container">
    <div class="well">
        <div class="row">
            <div class="col-md-6">
                <h4><b>{{ $product["name"] }}</b></h4>
                <p>{{ $product["description"] }}</p>
                <ul class="list-inline text-center">
                    <li><a href="#" class="btn btn-default btn-xs">Demo ausprobieren</a></li>
                    <li><a href="#" class="btn btn-info btn-xs">{{ $product["name"] }} kaufen</a></li>
                </ul>
            </div>
            <div class="col-md-6">
                <img src="{{ $product["img_path"] }}" alt="">
            </div>
        </div>
    </div>
    <div class="text-center">
        <h4>Funktionen von {{ $product["name"] }}</h4>
    </div>
</div>
@endsection