<div class="footer">
    <div class="container">
        <p class="text-muted credit">
            <a href="{{ url('legalnotice') }}">Impressum</a> &sdot;
            Copyright &copy; MyOnePager.net 2014. All Rights Reserved
        </p>
    </div>
</div>